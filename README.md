# soal-shift-sisop-modul-4-B08-2022

## Daftar Isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomor 1](#nomor-1)
    - Deskripsi Soal
    - [Kendala Nomor 1](#kendala-nomor-1)
    - [Solusi Nomor 1](#solusi-nomor-1)
    - [Source Code Nomor 1](https://gitlab.com/Anggito02/soal-shift-sisop-modul-4-b08-2022/-/tree/main/anya_B08.c)
- [Nomor 2](#nomor-2)
    - Deskripsi Soal
    - [Kendala Nomor 2](#kendala-nomor-2)
    - [Solusi Nomor 2](#solusi-nomor-2)
    - [Source Code Nomor 2](https://gitlab.com/Anggito02/soal-shift-sisop-modul-4-b08-2022/-/tree/main/anya_B08.c)
- [Nomor 3](#nomor-3)
    - Deskripsi Soal
    - [Kendala Nomor 3](#kendala-nomor-3)
    - [Solusi Nomor 3](#solusi-nomor-3)
    - [Source Code Nomor 3](https://gitlab.com/Anggito02/soal-shift-sisop-modul-4-b08-2022/-/tree/main/anya_B08.c)
    
## Anggota Kelompok

- Anggito Anju Hartawan Manalu	        5025201216
- Muhammad Yusuf Singgih Maulana	    5025201146
- Izzati Mukhammad	                    5025201075

## Nomor 1

### Deskripsi Soal
Kita harus membuat File System yang memiliki beberapa fungsi sebagai berikut.

(1.a) Direktori di dalam file system yang namanya berawalan "Animeku_" harus di encode atbash cipher untuk huruf besar dan rot13 untuk huruf kecil

(1.b) Direktori yang direname dengan awalan yang sama dengan 1.a juga di decode

(1.c) Jika direktori dengan awalan "Animeku_" di-rename, isi direktori tersebut di decode

(1.d) Setiap Encode dan Decode dimasukkan dalam Wibu.log

(1.e) Metode encode dan decode harus rekursif

### Kendala Nomor 1
Ditemukan kendala saat fuse di mount, terkadang terdapat bug dimana direktori fuse tidak bisa dibuka. Akan tetapi, saat sistem linux di-restart, bug tersebut hilang

### Solusi Nomor 1
(1.a) Kita menggunakan fungsi-fungsi fuse untuk mendapatkan atribut dari file yang ada di direktori berawalan "Animeku_" kemudian kita decode sesuai aturan yang diberikan. (1.e) Tidak lupa dilakukan secara rekursif

(1.b) Karena fuse dijalankan secara otomatis dan rekursif, saat rename dilakukan, file otomatis terdecode dan (1.c) terdecode jika di-rename lagi

(1.d) Kita dapat menggunakan FILE untuk membuka dan append isi log yang ingin dituliskan pada Wibu.log

## Nomor 2

### Deskripsi Soal
Soal pada nomor 2 memiliki kemiripan dengan nomor 1, hanya berbeda pada sistem encodenya dimana,

(2.a) direktori dengan awalan "IAN_" akan diencode dengan Vigenere Cipher dengan key "INNUGANTENG"

Sedangkan, (2.b) dan (2.c) memiliki kesamaan dengan nomor 1

(2.e) dan (2.f) menyuruh kita untuk membuat file log yang memiliki aturan nama "/home/[user]/hayolongapain_[kelompok].log" dan aturan-aturan sesuai yang tertulis pada soal

### Kendala Nomor 2
Kendala pada sistem fuse kurang lebih sama dengan nomor 1

### Solusi Nomor 2
(2.a) Digunakan logic yang sama untuk memecahkan problem nomor (1.a)

(2.b) dan (2.c) Diselesaikan dengan logic yang sama dengan nomor (2.b) dan (2.c)

(2.e) dan (2.f) Kita menggunakan FILE untuk membuka file log dan mengatur isinya sesuai dengan yang diinginkan oleh soal

## Nomor 3

### Deskripsi Soal

Karena Sin masih super duper gabut akhirnya dia menambahkan sebuah fitur lagi pada filesystem mereka.

(3.a) Jika sebuah direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial.

(3.b) Jika sebuah direktori di-rename dengan memberi awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial.

(3.c) Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

(3.d) Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing-masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

(3.e) Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi lowercase insensitive dan diberi ekstensi baru berupa nilai desimal dari binner perbedaan namanya.

Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan 
menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110


### Kendala Nomor 3
Untuk nomor 3 ini banyak sekali masalah yang ditemukan salah satunya yaitu ketika melakukan rename pada folder muncl pusan "Transport endpoint is not connected", pada akhirnya file system tidak dapat dijalankan sesuai dengan yang diinginkan

### Solusi Nomor 3

#### 3.a
Untuk mengecek apakah direktori merupakan direktori spesial atau tidak, fungsi `namdosaq()` digunakan. Fungsi ini menerima parameter sebuah string yaitu direktori yang akan di cek. Fungsi ini mengembalikan nilai TRUE jika direktori mempunyai substring "nam_do-saq_" dan mengembalikan nilai FALSE jika direktori tidak mempunyai substring "nam_do-saq_".

Selain fungsi namdosaq, ada fungsi `namdosaq_Content(path)`. path merupakan string yang merujuk pada lokasi sebuah file sehingga fungsi ini bertujuan untuk mengecek apakah suatu file berada di dalam direktori spesial atau tidak. Fungsi ini mengembalikan nilai TRUE jika parent direktori pada path mempunyai substring "nam_do-saq_". Parent direktori saja yang di cek karena pada soal 3 poin d dijelaskan bahwa direktori spesial hanya berdampak pada file-file yang berada di dalamnya. Dalam kata lain, sifat direktori spesial tidak diturunkan kepada folder yang ada di dalamnya secara rekursif.

#### 3.b
Fungsi fuse `xmp_rename()` akan terpanggil ketika sebuah direktori di-rename atau sebuah file di-rename. Dengan memanfaatkan fungsi isAnimeku yang telah dibuat pada poin a, kita dapat mengetahui apakah sebuah direktori di-rename dari direktori spesial menjadi direktori tidak spesial atau direktori tidak spesial menjadi direktori spesial dengan membandingan isAnimeku() dan isAnimeku().

#### 3.c
Penjelasan terkait poin c telah diberikan pada strategi penyelesaian poin b.

#### 3.d
Jika sebuah direktori yang mempunyai awalan Animeku_ atau IAN_ di-rename menjadi direktori spesial (direktori dengan awalan nam_do-saq_), maka file-file yang berada di dalam direktori tersebut akan di-decode. Akan tetapi, file-file yang berada di dalam folder yang berada di dalam direktori tersebut tidak di-decode.

Untuk menyelesaikan persoalan ini, fungsi xmp_rename digunakan. Dengan memanfaatkan fungsi isIAN dan isAnimeku yang telah dibuat, kita dapat mendeteksi kapan sebuah direktori IAN_ di-rename menjadi direktori spesial atau kapan sebuah direktori Animeku_ di-rename menjadi direktori spesial. Berikut ini contohnya

#### 3.d
Untuk poin 3, kami belum dapat mengimplementasikannya. Kami belum menemukan cara memanipulasi nama-nama file sesuai yang diminta.
